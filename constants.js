export const service = "https://sagecell.sagemath.org/service";

export const inputFields = ['e', 'r', 't', 'y', 'i', /*'x', 'f', */'m', 'v', 'g', 'w'];
export const resultFields = ['a','b','s','l','d',/* 'p','c','q', */'o'];

export const inputLabels = (field) => {
    switch(field) {
      case 'v':
        return 'Valuation ratio';
      // case 'f':
      //   return `Partner investment (.2 = 20% of down payment)`;
      case 'e':
        return 'Monthly expenses';
      case 'i':
        return 'Loan interest rate';
      case 'y':
        return 'Loan duration (years)';
      // case 'x':
      //   return `Partner's equity (.25 = %25 after tax cash flow)`;
      case 't':
        return 'Income tax rate';
      case 'r':
        return 'Required Down Payment (.4 = 40%)';
      case 'm':
        return 'Margin (.1 = 10% above needed)';
      case 'w':
        return 'Cash today toward down payment';
      case 'g':
        return 'Savings from each paycheck';
      default:
        return 'N/A';
    }
  };

  export const resultLabels = (field) => {
    switch(field) {
      case 'a':
        return 'After tax annual cash flow';
      case 'b':
        return 'Before tax annual cash flow';
      case 's':
        return 'Sale price';
      case 'l':
        return 'Monthly loan payment';
      case 'd':
        return 'Down payment';
      // case 'p':
      //   return `Partner's contribution to down payment`;
      // case 'c':
      //   return 'Cash (including future pay periods)';
      // case 'q':
      //   return 'Loan annuity discount factor';
      case 'o':
        return 'Paychecks to go before buying';
      default:
        return 'N/A';
    }
  };