import { connect } from 'react-redux';

import React, { Component } from 'react';
import { ActivityIndicator, Button, ScrollView, StyleSheet, StatusBar, View } from 'react-native';

import t from 'tcomb-form-native';

var RJSON = require("relaxed-json")

// Imports: Redux Actions
import { calculated } from './redux/actions/calculatedActions';
import { busy } from './redux/actions/busyActions';
import { setInputs } from './redux/actions/inputsActions';
import { setResults } from './redux/actions/resultsActions';

import { inputFields, resultFields, inputLabels, resultLabels, service } from './constants';

const Form = t.form.Form;

const Results = t.struct(resultFields.reduce((o, key) => ({ ...o, [key]: t.Number}), {}));
const Inputs = t.struct(inputFields.reduce((o, key) => ({ ...o, [key]: t.Number}), {}))

// https://github.com/you-dont-need/You-Dont-Need-Lodash-Underscore#_pick
const pick = (object, keys) => {
  return keys.reduce((obj, key) => {
     if (object && object.hasOwnProperty(key)) {
        obj[key] = object[key];
     }
     return obj;
   }, {});
};

const styles = StyleSheet.create({
  container: {
    flex:1,
    justifyContent: 'center',
    marginTop: StatusBar.currentHeight,
    padding: 20,
    backgroundColor: '#ffffff',
  },
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'
  }
});

const buildSageQuery = (inputs) => {
  const {e, r, t, y, i, m, v, g, w} = inputs;
  return encodeURIComponent(
    `a, b, s, e, l, d, r, t, y, i, p, c, m, v, o, q, g, w = var('a, b, s, e, l, d, r, t, y, i, p, c, m, v, o, q, g, w');print({k: v.n(digits=3) if v.is_numeric() else v for k, v in solve([w==${w},g==${g},y==${y},v==${v},m==${m},d==c,c==w+g*o,i==${i},t==${t},r==${r},a == b*(1-t),b==s/v,(1+m)*((e+l)*12)==a,e==${e},l==s*(1-r)/q,q==((1+i/12)^(y*12)-1)/(i/12*(1+i/12)^(y*12)),d==s*r],a, b, s, e, l, d, r, t, y, i, p, c, m, v, o, q, g, w, solution_dict=True)[0].items()})`
)};

const parseAnswer = json => {
  const preppedJson = json.stdout.replace(/\.,/g,",").replace(/\.}/g,"}");
  return pick(RJSON.parse(preppedJson), resultFields);
};
class BuyLaundromat extends Component {

  getResults = async (inputs) => {
    this.props.reduxBusy(true);
    const response = await fetch(service, {
    "headers": {
      "accept": "*/*",
      "accept-language": "en-US,en;q=0.9",
      "cache-control": "no-cache",
      "content-type": "application/x-www-form-urlencoded",
      "pragma": "no-cache",
      "sec-ch-ua-mobile": "?0",
      "sec-fetch-dest": "empty",
      "sec-fetch-mode": "cors",
      "sec-fetch-site": "cross-site"
    },
    "body": `code=${buildSageQuery(inputs)}`,
    "method": "POST",
    "mode": "cors",
    "credentials": "omit"
  });
  this.props.reduxBusy(false);
  return parseAnswer(await response.json());
  }
  
  handleSubmit = async () => {
      this.props.reduxCalculated(false);
      this.props.reduxSetResults(await this.getResults(this.props.inputs));
      this.props.reduxCalculated(true);
  }

  render() {
    const { busy } = this.props;
    return (
        <View style={styles.container}>
            <ActivityIndicator
              animating = {busy}
              color = '#34e8eb'
              size = "large"
              style={styles.loading}
            />
            <ScrollView contentContainerStyle={{flexGrow: 1, justifyContent: 'center'}} >
                <Form
                ref={c => this._form = c} // assign a ref
                type={Inputs} 
                value={this.props.inputs}
                onChange={(value) => { this.props.reduxSetInputs(value); this.props.reduxCalculated(false) } }
                options={{fields: inputFields.reduce((o, key) => ({ ...o, [key]: {label: inputLabels(key)}}), {})}} />
                <Button
                title="How 'much' laundromat should I buy?"
                onPress={this.handleSubmit}
                />
            {this.props.calculated && 
                <Form
                type={Results}
                value={this.props.results}
                options={{fields: resultFields.reduce((o, key) => ({ ...o, [key]: {label: resultLabels(key), editable: false}}), {})}}
                />
            }
            </ScrollView>
        </View>
    );
  }
}

// Map State To Props (Redux Store Passes State To Component)
const mapStateToProps = (state) => {
    // Redux Store --> Component
    return {
      inputs: state.inputsReducer.inputs,
      calculated: state.calculatedReducer.calculated,
      busy: state.busyReducer.busy,
      results: state.resultsReducer.results
    };
  };
  
  // Map Dispatch To Props (Dispatch Actions To Reducers. Reducers Then Modify The Data And Assign It To Your Props)
  const mapDispatchToProps = (dispatch) => {
    // Action
    return {
      // Set Inputs
      reduxSetInputs: (value) => dispatch(setInputs(value)),
      // Toggle Calculated
      reduxCalculated: (trueFalse) => dispatch(calculated(trueFalse)),
      // Toggle Busy
      reduxBusy: (trueFalse) => dispatch(busy(trueFalse)),
      // Set Results
      reduxSetResults: (value) => dispatch(setResults(value))
    };
  };
  // Exports
  export default connect(mapStateToProps, mapDispatchToProps)(BuyLaundromat);