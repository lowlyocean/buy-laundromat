// Set Inputs
export const setInputs = (value) => ({
    type: 'SET_INPUTS',
    inputs: value
  });