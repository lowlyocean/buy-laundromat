export const calculated = (trueFalse) => ({
    type: 'CALCULATED',
    trueFalse: trueFalse,
  });