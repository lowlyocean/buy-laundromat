import { inputFields } from '../../constants'

// Initial State
const init = (field) => {
  switch(field) {
    case 'v':
      return 3.2;
    case 'f':
      return 1;
    case 'e':
      return 2040.8;
    case 'i':
      return 0.07;
    case 'y':
      return 7;
    case 'x':
      return 0.0;
    case 't':
      return 0.17;
    case 'r':
      return .35;
    case 'm':
      return 0.0;
    case 'w':
      return 13900; //in the bank
    case 'g':
      return 114.25; //every other week, savings from paycheck
    default:
      return 0.0;
  }
};

const initialState = {
  inputs: inputFields.reduce((o, field) => ({ ...o, [field]: init(field)}), {})
}

// Reducers (Modifies The State And Returns A New State)
const inputsReducer = (state = initialState, action) => {
  switch (action.type) {
    // Set inputs
    case 'SET_INPUTS': {
      return {
        // State
        ...state,
        // Redux Store
        inputs: action.inputs,
      }
    }
    // Default
    default: {
      return state;
    }
  }
};
// Exports
export default inputsReducer;