// Initial State
const initialState = {
    calculated: false
  };

   // Reducers (Modifies The State And Returns A New State)
   const calculatedReducer = (state = initialState, action) => {
    switch (action.type) {
      // Calculated
      case 'CALCULATED': {
        return {
          // State
          ...state,
          // Redux Store
          calculated: action.trueFalse,
        }
      }
      // Default
      default: {
        return state;
      }
    }
  };
  // Exports
  export default calculatedReducer;