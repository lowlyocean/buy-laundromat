// Imports: Dependencies
import { combineReducers } from 'redux';
// Imports: Reducers
import inputsReducer from './inputsReducer';
import calculatedReducer from './calculatedReducer';
import busyReducer from './busyReducer';
import resultsReducer from './resultsReducer';

// Redux: Root Reducer
const rootReducer = combineReducers({
  inputsReducer: inputsReducer,
  calculatedReducer: calculatedReducer,
  busyReducer: busyReducer,
  resultsReducer: resultsReducer
});
// Exports
export default rootReducer;