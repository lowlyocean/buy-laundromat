//import { resultFields } from '../../constants'

// Initial State
const initialState = {
  results: {}//resultFields.reduce((o, field) => ({ ...o, [field]: undefined}), {})
}

  // Reducers (Modifies The State And Returns A New State)
  const resultsReducer = (state = initialState, action) => {
  switch (action.type) {
    // Set results
    case 'SET_RESULTS': {
      return {
        // State
        ...state,
        // Redux Store
        results: action.results,
      }
    }
    // Default
    default: {
      return state;
    }
  }
};
// Exports
export default resultsReducer;