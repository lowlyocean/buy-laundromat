// Initial State
const initialState = {
    busy: false
  };

   // Reducers (Modifies The State And Returns A New State)
   const busyReducer = (state = initialState, action) => {
    switch (action.type) {
      // Busy
      case 'BUSY': {
        return {
          // State
          ...state,
          // Redux Store
          busy: action.trueFalse,
        }
      }
      // Default
      default: {
        return state;
      }
    }
  };
  // Exports
  export default busyReducer;